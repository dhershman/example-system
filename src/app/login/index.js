'use strict';

const AC = require('../../../sky-modules/air-caste');
const GK = require('../../../sky-modules/gate-keeper');

exports.register = (server, options, next) => {

	server.route({
		path: '/login',
		method: 'POST',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			let keeper = GK(request, reply);
			keeper.manualLogin(request.payload.login_username, request.payload.login_password);

		}
	});

	server.route({
		path: '/logout',
		method: 'GET',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			return reply().unstate('token').redirect('/home');
		}
	});

	server.route({
		path: '/loginpage',
		method: 'GET',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			return require('./page')(request, reply);
		}
	});

	server.route({
		path: '/register',
		method: 'POST',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			let keeper = GK(request, reply);
			keeper.addNewAccount({
				user: request.payload.user,
				email: request.payload.email,
				pass: request.payload.pass
			});
		}
	});

	next();
};

exports.register.attributes = {
	pkg: require('./package.json')
};
