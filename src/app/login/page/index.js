'use strict';

module.exports = (request, reply) => {
	reply.view('login/page/index', {
		page: {
			title: 'Login',
			id: 'login-page'
		},
		js: {
			page: 'register.js'
		},
		css: {
			page: 'default'
		}
	});
};