'use strict';

module.exports = (request, reply) => {
	reply.view('landing/home/index', {
		page: {
			title: 'Home',
			id: 'home'
		},
		user: request.auth.credentials.id,
		css: {
			page: 'default'
		}
	});
};