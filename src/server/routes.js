'use strict';

const path = require('path');

module.exports = (server) => {

	server.route({
		method: 'GET',
		path: '/assets/{param*}',
		config: {
			auth: false
		},
		handler: {
			directory: {
				path: './public'
			}
		}
	});

	server.route({
		path: '/{path*}',
		method: ['GET', 'POST'],
		handler: (request, reply) => {
			let loc = (request.path) ? request.path : '';
			return require('../app' + loc)(request, reply);
		}
	});

	return server;

};
