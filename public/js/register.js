$(function() {
	var $page = $('.login-page');
	var $register = $page.find('#register');
	var $login = $page.find('#login');

	$register.hide();
	$page.on('click', '#switchLink', formSwitch);

	function formSwitch(e) {
		var current = $(e.currentTarget).data('current');
		$('#' + current).fadeOut('fast', function() {
			if (current === 'login') {
				$register.fadeIn('fast');
				$(e.currentTarget).text('Already Registered? Click here to Login!');
				$(e.currentTarget).data('current', 'register');
			} else {
				$login.fadeIn('fast');
				$(e.currentTarget).text('No Account? Click here to register!');
				$(e.currentTarget).data('current', 'login');
			}
		});
	}
});