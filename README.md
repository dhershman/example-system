# README #

Simple Login app using a rethinkdb database system, JSON Web Tokens, and bootstrap!

### How do I get set up? ###

* Clone this repo
* Open Console/Terminal in repo directory
* type npm i
* run rethinkdb.exe
* type node .

### Notes ###

* Uses a prototype module I am building for account handling (it has inline db calls which is a no no but was easiest until its finished)
* Login, logout, and it displays some info