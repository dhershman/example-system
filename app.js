'use strict';

const hapi = require('hapi');
const r = require('rethinkdb');
const server = new hapi.Server();
server.connection({
	port: 5000
});

//Before initiating our server lets go ahead and make sure our database is setup
//check if our database exists create it if not and then we check if our accounts table exists.
r.connect({host: 'localhost', port: 28015}, (err, conn) => {
	r.dbList().run(conn).then((dbExists) => {
		if (dbExists.indexOf('test') === -1) {
			r.dbCreate('test').run(conn);
		}
		r.db('test').tableList().run(conn).then((tableNames) => {
			if (tableNames.indexOf('accounts') === -1) {
				r.tableCreate('accounts').run(conn);
			}
		});
	});
});
//Easy Peasy!

//This won't bother the plugin anyway since it should just overwrite our connection and place it in our plugins
//
//Sending server.register an array of plugins we want to use
server.register([{
		register: require('hapi-rethinkdb'),
		options: {
			url: 'rethinkdb://localhost:28015/test'
		}
	},
	require('./sky-modules/inquisitor'),
	require('vision'),
	require('inert'),
	require('./src/app/landing'),
	require('./src/app/login')

], (err) => {
	if (err) throw err;

	require('./src/server/routes.js')(server);

	server.views({
		engines: {
			hbs: require('handlebars')
		},
		path: './src/app',
		layoutPath: './src/templates/layouts',
		partialsPath: './src/templates/partials',
		layout: 'main'
	});

	server.ext('onPreResponse', (request, reply) => {

		if(request.response.isBoom) {
			switch(request.response.output.statusCode) {
				case 401: //Unauthorized
					return reply.redirect('/loginpage');
				case 500: //Bad Request
					break;
				case 404: //Not Found
					break;
				case 422: //Bad Data
					break;
				case 409: //Conflict
					break;
				default:
					break;
			}
		}
		reply.continue();
	});

	server.start((err) => {
		if (err) throw err;
		console.log('Server running at: ' + server.info.uri);
		server.log('info', 'Server running at: ' + server.info.uri);
	});

});